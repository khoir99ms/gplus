__author__ = 'Khoir MS'

import re
from browser import Browser
from endpoints import Endpoints

class Gplus(Browser):

    def __init__(self, **kwargs):
        super(Gplus, self).__init__(**kwargs)

        self.__apikey = ''
        self.__access_token = ''
        self.is_credential_set = False
        self.response = None

    def set_credential(self, apikey='', access_token=''):
        self.log('setup credential key ...')
        self.__apikey = apikey
        self.__access_token = access_token
        if apikey:
            self.log('using api key {}'.format(apikey))
        if access_token:
            self.log('using access token {}'.format(apikey))

    def search_people(self, query, max_result=50, page_token='', **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.search_people(query=query, max_result=max_result,
                                           page_token=page_token, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def search_activity(self, query, max_result=20, page_token='', **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.search_activity(query=query, max_result=max_result,
                                           page_token=page_token, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def people_info(self, user_id, **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.people_info(user_id=user_id, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def activities(self, user_id, max_result=100, page_token='', **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.activities(user_id=user_id, max_result=max_result, page_token=page_token, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def activity_detail(self, activity_id, **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.activity_detail(activity_id=activity_id, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def comments(self, activity_id, max_result=500, order='descending', page_token='', **kwargs):
        timeout = kwargs.setdefault('timeout', 60)
        proxies = kwargs.setdefault('proxies', dict())

        self.__is_credential_set()
        endpoint = Endpoints.comments(activity_id=activity_id, max_result=max_result, order=order,
                                      page_token=page_token, apikey=self.__apikey, access_token=self.__access_token)
        self.set_proxies(proxies)
        self.response = self.session.get(endpoint, timeout=timeout)
        if self.response.status_code != 200:
            self.log('request error, please check your connection !', level='error')
            self.get_api_errors()
        receive = self.response.json()

        return receive

    def __is_credential_set(self):
        if not (self.__apikey or self.__access_token):
            raise Exception('please set credential key using apikey or access_token')

        self.is_credential_set = True

    def get_api_errors(self):
        eresponse = self.response.json()
        ecode = eresponse['error']['code']
        emessage = eresponse['error']['message']

        if ecode == 403:
            reason = eresponse['error']['errors'][0]['reason']
            if re.search(r'dailyLimitExceeded', reason, flags=re.I):
                raise DailyLimitExcedeed(emessage)

        if ecode == 404:
            reason = eresponse['error']['errors'][0]['reason']
            if re.search(r'notFound', reason, flags=re.I):
                raise NotFoundAccount(emessage)

        if ecode != 200:
            raise BaseApiException(emessage)

class DailyLimitExcedeed(Exception):
    pass

class NotFoundAccount(Exception):
    pass

class BaseApiException(Exception):
    pass

if __name__=="__main__":
    gp = Gplus()

    gp.set_credential(apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
    # data = gp.search_people(query="the star", page_token='CDISFTEwMDYyMDg0MzE0NzU0OTI1ODY3OQ')
    try:
        # data = gp.people_info(user_id='115541631212232428971')
        data = gp.search_activity(query='"kota batu"')
        print data
    except NotFoundAccount:
        pass
    except Exception:
        pass
        # print gp.response.json()