__author__ = 'Khoir MS'

import urlparse
import urllib

class Endpoints:

    __BASE_URL = 'https://www.googleapis.com'
    __API_URL = 'https://www.googleapis.com/plus/v1'
    __PEOPLE_SEARCH = '{api_url}/people?query={query}&maxResults={max_result}&pageToken={page_token}&access_token={access_token}&key={apikey}'
    __PEOPLE_INFO = '{api_url}/people/{user_id}?access_token={access_token}&key={apikey}'
    __ACTIVITY_LIST = '{api_url}/people/{user_id}/activities/public?access_token={access_token}&key={apikey}'
    __ACTIVITY_DETAIL = '{api_url}/activities/{activity_id}?maxResults={max_result}&pageToken={page_token}&access_token={access_token}&key={apikey}'
    __ACTIVITY_SEARCH = '{api_url}/activities?query={query}&maxResults={max_result}&pageToken={page_token}&access_token={access_token}&key={apikey}'
    __COMMENT_LIST = '{api_url}/activities/{activity_id}/comments?maxResults={max_result}&sortOrder={order}&pageToken={page_token}&access_token={access_token}&key={apikey}'
    __COMMENT_DETAIL = '{api_url}/comments/{comment_id}?access_token={access_token}&key={apikey}'

    def __init__(self):
        pass

    @staticmethod
    def search_people(query, max_result=50, page_token='', access_token='', apikey=''):
        if max_result < 1 or max_result > 50: max_result = 50
        endpoint = Endpoints.__PEOPLE_SEARCH.format(api_url=Endpoints.__API_URL, query=query, max_result=max_result, page_token=page_token, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def search_activity(query, max_result=20, page_token='', access_token='', apikey=''):
        if max_result < 1 or max_result > 20: max_result = 20
        endpoint = Endpoints.__ACTIVITY_SEARCH.format(api_url=Endpoints.__API_URL, query='"{}"'.format(query), max_result=max_result,
                                                      page_token=page_token, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def people_info(user_id, access_token='', apikey=''):
        endpoint = Endpoints.__PEOPLE_INFO.format(api_url=Endpoints.__API_URL, user_id=user_id, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def activities(user_id, max_result=100, page_token='', access_token='', apikey=''):
        endpoint = Endpoints.__ACTIVITY_LIST.format(api_url=Endpoints.__API_URL, user_id=user_id, max_result=max_result, page_token=page_token, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def activity_detail(activity_id, access_token='', apikey=''):
        endpoint = Endpoints.__ACTIVITY_DETAIL.format(api_url=Endpoints.__API_URL, activity_id=activity_id, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def comments(activity_id, max_result=500, order='descending', page_token='', access_token='', apikey=''):
        if max_result < 1 or max_result > 500: max_result = 500
        endpoint = Endpoints.__COMMENT_LIST.format(api_url=Endpoints.__API_URL, activity_id=activity_id, max_result=max_result,
                                                   order=order, page_token=page_token, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def comment_detail(comment_id, access_token='', apikey=''):
        endpoint = Endpoints.__COMMENT_DETAIL.format(api_url=Endpoints.__API_URL, comment_id=comment_id, access_token=access_token, apikey=apikey)
        parseurl = urlparse.urlsplit(endpoint)
        qstring = urllib.urlencode(Endpoints.parse_qstring(endpoint))

        return "{base_url}{path}?{query}".format(base_url=Endpoints.__BASE_URL, path=parseurl.path, query=qstring)

    @staticmethod
    def parse_qstring(url):
        return dict(urlparse.parse_qsl(urlparse.urlsplit(url).query))

if __name__=='__main__':
    print Endpoints.search_people(query="dede", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
    print Endpoints.people_info(user_id="dede", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
    print Endpoints.activities(user_id="1234", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
    print Endpoints.activity_detail(activity_id="1234", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
    print Endpoints.search_activity(query="dede", apikey='AIzaSyBn5o04MbZoUlgmCqyZFdcSgHbxM9UQoEM')
